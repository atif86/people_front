(function() {
  'use strict';

  angular
    .module('atif')
    .controller('LoginController', LoginController);

  /** @ngInject */
  function LoginController($timeout, webDevTec, toastr, $location, $http) {
    var vm = this;

    vm.awesomeThings = [];
    vm.task = '';
    vm.email = '';
    vm.password = '';
    vm.classAnimation = '';
    vm.creationDate = 1499438228546;
    vm.buttonPress = buttonPress;
    // vm.addTask = addTask;

    function buttonPress() {
      var loginData = {
        email: vm.email,
        password: vm.password
      }

      $http({
        method: 'POST',
        url: 'http://localhost:8080/api/loginUser',
        data: loginData
      })
      .then(function(res) {
        if(res.data.message === 'success') {
          $location.path('/main');
        }
      })
      .catch(function() {
        toastr.error('Error while login', 'Error');
      })
    }

  }
})();
