(function() {
  'use strict';

  angular
    .module('atif', ['ngAnimate', 'ngCookies', 'ngSanitize', 'ngMessages', 'ngAria', 'restangular', 'ngRoute', 'toastr', 'ngMaterial', 'react']);

})();
