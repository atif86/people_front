/* global malarkey:false, moment:false */
(function() {
  'use strict';

  angular
    .module('atif')
    .constant('malarkey', malarkey)
    .constant('moment', moment);

})();
