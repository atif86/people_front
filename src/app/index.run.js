(function() {
  'use strict';

  angular
    .module('atif')
    .run(runBlock);

  /** @ngInject */
  function runBlock($log) {

    $log.debug('runBlock end');
  }

})();
