(function() {
  'use strict';

  var HelloComponent = React.createClass({
    displayName: 'HelloComponent',
    propTypes: {},
    render: function render() {
      return React.DOM.span( null,
        'Hello ' + this.props.fname + ' ' + this.props.lname
      );
    }
  });

  angular.module('atif').value('HelloComponent', HelloComponent);
})();
