(function() {
  'use strict';

  angular
    .module('atif')
    .controller('MainController', MainController);

  /** @ngInject */
  function MainController($timeout, webDevTec, toastr, $location, $http, moment) {
    var vm = this;

    vm.awesomeThings = [];
    vm.loginData = {};
    vm.task = '';
    vm.classAnimation = '';
    vm.creationDate = 1499438228546;
    vm.addTask = addTask;
    vm.editClick = editClick;
    vm.myDate = new Date();
    vm.list = [];
    vm.startTime = '';
    vm.endTime = '';
    vm.person = { fname: 'atif', lname: 'sayed'};
    vm.getTask = getTask;

    getTaskByDate();

    function addTask() {
      // console.log('startTime', moment(vm.myDate).format('YYYY-MM-DD'));
      var taskData = {
        description: vm.task,
        start_time: moment(vm.startTime).format('hh:mm'),
        end_time: moment(vm.endTime).format('hh:mm'),
        user_id: 1,
        date: moment(vm.myDate).format('YYYY-MM-DD')
      }
      $http({
        method: "POST",
        url: "http://localhost:8080/api/task/addTask",
        data: taskData
      }).then(function(res) {
        if(moment().format('YYYY-MM-DD') === res.data.date){
          vm.list.push(res.data);
        }
        clearInput();
        toastr.success('Task Added', 'Success');
      })
      .catch(function() {
        toastr.error('Adding task went wrong', 'Error');
      })
    }

    function clearInput() {
      vm.task = '';
      vm.startTime = '';
      vm.endTime = '';
      vm.myDate = new Date();
    }

    function getTaskByDate() {
      $http({
        method:"POST",
        url: "http://localhost:8080/api/task/getTaskByDate",
        data: { date: moment().format('YYYY-MM-DD')}
      }).then(function(res){
        vm.list = res.data
      })
      .catch(function(){
        toastr.error('Fetching task went wrong', 'Error');
      })
    }

    function getTask() {
      $http({
        method:"POST",
        url: "http://localhost:8080/api/task/getTaskByDate",
        data: { date: moment(vm.myDate).format('YYYY-MM-DD')}
      }).then(function(res){
        vm.list = res.data
      })
      .catch(function(){
        toastr.error('Fetching task went wrong', 'Error');
      })
    }

    function editClick(id) {
      // console.log('editClick', id);
    }
  }
})();
